" PLUGINS ---------------------------------------------------------------- {{{

" Plugin code goes here.
call plug#begin('~/.vim/plugged')

    Plug 'morhetz/gruvbox'
    Plug 'dense-analysis/ale'
    Plug 'vim-airline/vim-airline'
    Plug 'sheerun/vim-polyglot'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'

call plug#end()


" }}}

"PlugInstall
"PlugUpdate
"PlugStatus
"PlugClean
"PlugUpgrade
