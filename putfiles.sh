(
  cp .aliases ~
  cp .bashrc ~
  cp .bash_profile ~
  cp -a ./.config/* ~/.config/
  cp .dircolors ~
  cp ./.git-completion.bash ~
  cp ./.gitconfig ~
  cp .gitignore_global ~
  cp .hyper.js ~
  cp -a .hyper_plugins/* ~/.hyper_plugins/
  cp ./.ssh/config ~/.ssh/
  cp ./starship.toml ~
  cp -a ./.vim/*.vim ~/.vim/
  cp -a ./.vim/colors/* ~/.vim/colors/
  cp .vimrc ~
  cp -a ./.zsh/. ~/.zsh
  cp .zshrc ~
#  cp dircolors-solarized.ansi-dark ~
)
