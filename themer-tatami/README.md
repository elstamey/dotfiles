# `themer`

Your theme's unique URL:

https://themer.dev/?colors.dark.accent0=%23d2381c&colors.dark.shade0=%236d875a&colors.dark.shade7=%23a0ba6c&colors.dark.accent1=%23e1f2b0&colors.dark.accent2=%23c7b26f&colors.dark.accent3=%233a5a51&colors.dark.accent4=%23e8e38a&colors.dark.accent5=%23ade0e7&colors.dark.accent6=%23fef4f4&colors.dark.accent7=%23d7b9e2&colors.light.shade0=%23e0ebae&colors.light.shade7=%236d875a&colors.light.accent0=%23f4b2c2&colors.light.accent1=%2374315c&colors.light.accent2=%23dbd0e5&colors.light.accent3=%2347885d&colors.light.accent4=%23feec46&colors.light.accent5=%23a2d6dd&colors.light.accent6=%23ffdb52&colors.light.accent7=%23dbd0e5&activeColorSet=light&calculateIntermediaryShades.dark=true

If you find `themer` useful, here are some ways to support the project:

- Star [`themer` on GitHub](https://github.com/themerdev/themer)
- Follow [@themerdev](https://twitter.com/themerdev) on Twitter
- [Send a tip through the Brave Browser](https://brave.com/the537), either on [the repository page](https://github.com/themerdev/themer) or [the Web UI](https://themer.dev)
- Pay what you want when downloading your theme from [themer.dev](https://themer.dev)
- [Sponsor the @themerdev GitHub org](https://github.com/sponsors/themerdev)

# Installation instructions

## Block Wave Wallpaper

Files generated:

* `Block Wave Wallpaper/themer-wallpaper-block-wave-dark-945x872.png`
* `Block Wave Wallpaper/themer-wallpaper-block-wave-light-945x872.png`

## Burst Wallpaper

Files generated:

* `Burst Wallpaper/themer-wallpaper-burst-dark-945x872.png`
* `Burst Wallpaper/themer-wallpaper-burst-light-945x872.png`

## Chrome

1. Launch Chrome and go to `chrome://extensions`.
2. Check the "Developer mode" checkbox at the top.
3. Click the "Load unpacked extension..." button and choose the desired theme directory (`Chrome/Themer Dark` or `Chrome/Themer Light`).

(To reset or remove the theme, visit `chrome://settings` and click "Reset to Default" in the "Appearance" section.)

## Circuits Wallpaper

Files generated:

* `Circuits Wallpaper/themer-wallpaper-circuits-dark-945x872.png`
* `Circuits Wallpaper/themer-wallpaper-circuits-light-945x872.png`

## Diamonds Wallpaper

Files generated:

* `Diamonds Wallpaper/themer-wallpaper-diamonds-dark-945x872.png`
* `Diamonds Wallpaper/themer-wallpaper-diamonds-light-945x872.png`

## Dot Grid Wallpaper

Files generated:

* `Dot Grid Wallpaper/themer-wallpaper-dot-grid-dark-945x872-1.png`
* `Dot Grid Wallpaper/themer-wallpaper-dot-grid-dark-945x872-2.png`
* `Dot Grid Wallpaper/themer-wallpaper-dot-grid-light-945x872-1.png`
* `Dot Grid Wallpaper/themer-wallpaper-dot-grid-light-945x872-2.png`

## Firefox Add-on

To use the generated extension package, the code will need to be packaged up and signed by Mozilla.

To package the code in preparation for submission, the `web-ext` tool can be used:

    npx web-ext build --source-dir 'Firefox Add-on/Themer Dark' # or 'Firefox Add-on/Themer Light'

Then the package can be submitted to Mozilla for review in the [Add-on Developer Hub](https://addons.mozilla.org/en-US/developers/addon/submit/distribution).

Learn more about Firefox themes from [extensionworkshop.com](https://extensionworkshop.com/documentation/themes/)

To theme Firefox without the need to create a developer account and go through the extension review process, see themer's integration with [Firefox Color](https://color.firefox.com).

## Firefox Color

The Firefox Color add-on allows for simple theming without the need for a developer account or package review process by Mozilla.

1. Install the [Firefox Color add-on](https://addons.mozilla.org/en-US/firefox/addon/firefox-color/).
2. Open 'Firefox Color/themer-dark.url' or 'Firefox Color/themer-light.url' directly with Firefox.
3. Click "Yep" when prompted to apply the custom theme.

For a more fully featured Firefox theme, see themer's Firefox theme add-on generator.

## Hyper

First, copy (or symlink) the outputted package directories to the Hyper local plugins directory:

    cp -R 'Hyper/themer-hyper-dark' ~/.hyper_plugins/local/
    cp -R 'Hyper/themer-hyper-light' ~/.hyper_plugins/local/

Then edit `~/.hyper.js` and add the package to the `localPlugins` array:

    ...
    localPlugins: [
      'themer-hyper-dark' // or 'themer-hyper-light'
    ],
    ...

## Octagon Wallpaper

Files generated:

* `Octagon Wallpaper/themer-wallpaper-octagon-dark-945x872.png`
* `Octagon Wallpaper/themer-wallpaper-octagon-light-945x872.png`

## Shirts Wallpaper

Files generated:

* `Shirts Wallpaper/themer-wallpaper-shirts-dark-945-872.png`
* `Shirts Wallpaper/themer-wallpaper-shirts-light-945-872.png`

## Triangles Wallpaper

Files generated:

* `Triangles Wallpaper/themer-wallpaper-triangles-dark-945x872.png`
* `Triangles Wallpaper/themer-wallpaper-triangles-light-945x872.png`

## Trianglify Wallpaper

Files generated:

* `Trianglify Wallpaper/themer-wallpaper-trianglify-dark-945x872-0.75-1.png`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-dark-945x872-0.75-2.png`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-light-945x872-0.75-1.png`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-light-945x872-0.75-2.png`

## Vim

Copy or symlink `Vim/ThemerVim.vim` to `~/.vim/colors/`.

Then set the colorscheme in `.vimrc`:

    " The background option must be set before running this command.
    colo ThemerVim