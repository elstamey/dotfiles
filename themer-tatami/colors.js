// This file can be used with the themer CLI, see https://github.com/themerdev/themer

module.exports.colors = {
  "dark": {
    "shade0": "#6D875A",
    "shade7": "#A0BA6C",
    "accent0": "#D2381C",
    "accent1": "#E1F2B0",
    "accent2": "#C7B26F",
    "accent3": "#3A5A51",
    "accent4": "#E8E38A",
    "accent5": "#ADE0E7",
    "accent6": "#FEF4F4",
    "accent7": "#D7B9E2"
  },
  "light": {
    "shade0": "#E0EBAE",
    "shade7": "#6D875A",
    "accent0": "#F4B2C2",
    "accent1": "#74315C",
    "accent2": "#DBD0E5",
    "accent3": "#47885D",
    "accent4": "#FEEC46",
    "accent5": "#A2D6DD",
    "accent6": "#FFDB52",
    "accent7": "#DBD0E5"
  }
};

// Your theme's URL: https://themer.dev/?colors.dark.accent0=%23d2381c&colors.dark.shade0=%236d875a&colors.dark.shade7=%23a0ba6c&colors.dark.accent1=%23e1f2b0&colors.dark.accent2=%23c7b26f&colors.dark.accent3=%233a5a51&colors.dark.accent4=%23e8e38a&colors.dark.accent5=%23ade0e7&colors.dark.accent6=%23fef4f4&colors.dark.accent7=%23d7b9e2&colors.light.shade0=%23e0ebae&colors.light.shade7=%236d875a&colors.light.accent0=%23f4b2c2&colors.light.accent1=%2374315c&colors.light.accent2=%23dbd0e5&colors.light.accent3=%2347885d&colors.light.accent4=%23feec46&colors.light.accent5=%23a2d6dd&colors.light.accent6=%23ffdb52&colors.light.accent7=%23dbd0e5&activeColorSet=light&calculateIntermediaryShades.dark=true
