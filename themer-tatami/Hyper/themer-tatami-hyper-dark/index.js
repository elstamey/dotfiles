
        module.exports.decorateConfig = config => {
          return Object.assign({}, config, {
            cursorColor: 'rgba(254, 244, 244, 0.5)',
            cursorAccentColor: '#6d875a',
            foregroundColor: '#99b369',
            backgroundColor: '#6d875a',
            selectionColor: 'rgba(173, 224, 231, 0.09999999999999998)',
            borderColor: '#e8e38a',
            colors: {
              black: '#6d875a',
              red: '#d2381c',
              green: '#3a5a51',
              yellow: '#c7b26f',
              blue: '#ade0e7',
              magenta: '#d7b9e2',
              cyan: '#e8e38a',
              white: '#99b369',
              lightBlack: '#748e5d',
              lightRed: '#e1f2b0',
              lightGreen: '#3a5a51',
              lightYellow: '#c7b26f',
              lightBlue: '#ade0e7',
              lightMagenta: '#d7b9e2',
              lightCyan: '#e8e38a',
              lightWhite: '#a0ba6c',
            },
          });
        };
      