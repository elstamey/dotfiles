
        module.exports.decorateConfig = config => {
          return Object.assign({}, config, {
            cursorColor: 'rgba(255, 219, 82, 0.5)',
            cursorAccentColor: '#e0ebae',
            foregroundColor: '#7d9566',
            backgroundColor: '#e0ebae',
            selectionColor: 'rgba(162, 214, 221, 0.09999999999999998)',
            borderColor: '#feec46',
            colors: {
              black: '#e0ebae',
              red: '#f4b2c2',
              green: '#47885d',
              yellow: '#dbd0e5',
              blue: '#a2d6dd',
              magenta: '#dbd0e5',
              cyan: '#feec46',
              white: '#7d9566',
              lightBlack: '#d0dda2',
              lightRed: '#74315c',
              lightGreen: '#47885d',
              lightYellow: '#dbd0e5',
              lightBlue: '#a2d6dd',
              lightMagenta: '#dbd0e5',
              lightCyan: '#feec46',
              lightWhite: '#6d875a',
            },
          });
        };
      