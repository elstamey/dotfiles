export PATH=/opt/homebrew/bin:$PATH
eval "$(starship init zsh)"
#source ~/.zsh_history
source ~/.aliases

source ~/.zsh/completion.zsh
source ~/.zsh/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/key-bindings.zsh

# Initialize the completion system
autoload -Uz compinit

# Cache completion if nothing changed - faster startup time
typeset -i updated_at=$(date +'%j' -r ~/.zcompdump 2>/dev/null || stat -f '%Sm' -t '%j' ~/.zcompdump 2>/dev/null)
if [ $(date +'%j') != $updated_at ]; then
  compinit -i
else
  compinit -C -i
fi

# Enhanced form of menu completion called `menu selection'
zmodload -i zsh/complist

# function to set the tab title
function set-title(){
    echo -ne "\033]0; $(PWD | sed -e "s;^$HOME;~;" | sed -e "s;^~/Code/CuriosityStream/;CS Dev ;" | sed -e "s;^~/Code/;CODE ;" | sed -e "s;^centos; $HOSTNAME ;" ) \007"
}
precmd_functions+=(set-title)

# determine versions of PHP installed with HomeBrew
installedPhpVersions=($(brew ls --versions | ggrep -E 'php(@.*)?\s' | ggrep -oP '(?<=\s)\d\.\d' | uniq | sort))

# create alias for every version of PHP installed with HomeBrew
for phpVersion in ${installedPhpVersions[*]}; do
    value="{"

    for otherPhpVersion in ${installedPhpVersions[*]}; do
        if [ "${otherPhpVersion}" = "${phpVersion}" ]; then
            continue
        fi

        # unlink other PHP version
        value="${value} brew unlink php@${otherPhpVersion};"
    done

    # link desired PHP version
    value="${value} brew link php@${phpVersion} --force --overwrite; brew services restart php@${phpVersion}; pecl install xdebug; } &> /dev/null && php -v"

    alias "${phpVersion}"="${value}"
done

alias refresh="source ~/.zshrc"

export PATH="/usr/local/sbin:$PATH"
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

export NVM_DIR=~/.nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/zsh_completion" ] && \. "$NVM_DIR/zsh_completion"  # This loads nvm zsh_completion

  if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

    autoload -Uz compinit
    compinit
  fi
# The following lines have been added by Docker Desktop to enable Docker CLI completions.
fpath=(/Users/emily.stamey/.docker/completions $fpath)
autoload -Uz compinit
compinit
# End of Docker CLI completions
