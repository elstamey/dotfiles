
# .bash_profile
 
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

#vm-key authorization socket
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval $(ssh-agent -s)
    ssh-add
fi

# User specific environment and startup programs
 
PATH="$HOME/bin:$HOME/.local/bin:/usr/bin:/usr/local/bin:$PATH"
#/c/Program\ Files/Oracle/VirtualBox/

export PATH
export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH=/opt/homebrew/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
