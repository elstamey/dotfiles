# .bashrc
export PATH=/opt/homebrew/bin:$PATH

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Source global definitions
if [ -f ~/.git-completion.bash ]; then
        source ~/.git-completion.bash
fi

# Source the .nvm dir
if [ -r ~/.nvm ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
fi

#alias homestead for anywhere on the machine
function homestead() {
    ( cd ~/envs/Homestead && vagrant $* )
}

# function to set the tab title
function set-title() {
    if [[ -z "$ORIG" ]]; then
      ORIG=$PS1
    fi
    TITLE="\[\e]2;$*\a\]"
    PS1=${ORIG}${TITLE}
}

# function to save only the last 1000 lines of log file
function shorten-log() {
  if [[ -z "$1" ]]
  then
    echo "usage: shorten-log {line-count} {path-to-logfile}"
    return
  fi

  echo "$(tail -${1} $2)" > ${2}

}

#run ovpn and set the terminal's tab title
function ovpn() {
    ( set-title OpenVPN )
    ( sudo openvpn --config /home/elstamey/Downloads/client.ovpn --auth-user-pass /home/elstamey/Code/inq.vpn )
}

function parse_git_branch() {
    ref=$(git-symbolic-ref HEAD 2> /dev/null) || return
    echo "("${ref#refs/heads/}")"
}

#reseed databases for project
function reseed() {
  if [[ -z "$1" ]]
  then
    echo "usage: reseed {dbname}"
    return
  fi
  mysql -u root $1 <<EOT
  drop database $1;
  create database $1;
  use $1;
  source $(githome)/database/seeds/${1}.sql
EOT
}

# testytest stuff
function checkStartPhantomJS() {
  if [[ -z "$(pgrep phantomjs)" ]]
  then
    echo "PhantomJS headless browser not running. Starting webdriver on port 4444...";
    nohup /usr/local/bin/phantomjs --webdriver=4444 --ignore-ssl-errors=true --ssl-protocol=any &
  fi
}

function acceptconsole() {
  $(githome)/vendor/bin/codecept console acceptance
}

function unittest() {
  if [[ "$1" = "--all" ]]
  then
    cept run unit
  else
    testytest "unit" $*
  fi
}

function accepttest() {
  if [[ "$1" != "-e" ]]
  then
    checkStartPhantomJS
  fi
  if [[ "$1" = "--all" ]]
  then
    cept run acceptance
  else
    testytest "acceptance" $*
  fi
}

function functest() {
  if [[ "$1" = "--all" ]]
  then
    cept run functional
  else
    testytest "functional" $*
  fi
}

function testytest() {
  editmode=false
  testtype=${1:-unit}
  leftpatternmatch=${2}
  if [[ "$leftpatternmatch" = "-e" ]]
  then
    editmode=true
    shift
    leftpatternmatch=${2}
  fi
  matchingfiles=$(cd $(viewhome)/tests/${testtype}; find . -name "${leftpatternmatch}*Test.php" -o -name "${leftpatternmatch}*Cept.php" -o -name "${leftpatternmatch}*Cest.php" | sed 's#^\./##g;';)
  number=$(echo $matchingfiles | wc -w)
  if [[ $number -eq 1 ]]
  then
    if $editmode
    then
      (cd $(viewhome)/tests/${testtype}; /usr/bin/vim $matchingfiles;)
    fi
    echo $matchingfiles
    (cd $(viewhome); cept --debug run ${testtype} $matchingfiles;)
  else
    select ut in $matchingfiles
    do
      case "$ut" in
      'q'|'')
        break;
      ;;
      *)
        clear;
        if $editmode
        then
          (cd $(viewhome)/tests/${testtype}; /usr/bin/vim $ut;)
        fi
        echo $ut;
        lasttest="$ut"
        (cd $(viewhome); cept --debug run ${testtype} $ut;)
      ;;
      esac
    done
  fi
}


# added by travis gem
[ -f C:/Users/elsta/.travis/travis.sh ] && source C:/Users/elsta/.travis/travis.sh


# alias for AWS mysql query
alias awsmysql='mysql --host=inquest.cluster-czptqaijecgf.us-east-1.rds.amazonaws.com --port=3306 --user=inquest --password="h=w49sHtw;YvW%X7WEuQ"'
alias awsmysqldump='mysqldump --host=inquest.cluster-czptqaijecgf.us-east-1.rds.amazonaws.com --port=3306 --user=inquest --password="h=w49sHtw;YvW%X7WEuQ" --databases inquest --ignore-table=inquest.users --ignore-table=inquest.users_data --ignore-table=inquest.users_group --ignore-table=inquest.users_password --ignore-table=inquest.users_recent_failed_login_attempt --ignore-table=inquest.users_session --ignore-table=inquest.sentry --ignore-table=inquest.sentry_health --ignore-table=inquest.sentry_health_history --ignore-table=inquest.sentry_log --ignore-table=inquest.sentry_option_value --add-drop-table --add-drop-trigger | gzip > review.sql.gz'

function awsmysqldumptable() {
	echo -e " mysqldump --host=inquest.cluster-czptqaijecgf.us-east-1.rds.amazonaws.com --port=3306 --user=inquest --password=\"h=w49sHtw;YvW%X7WEuQi\" --add-drop-table --add-drop-trigger --single-transaction=TRUE $1 $2 > $2.sql "

}

# determine versions of PHP installed with HomeBrew
installedPhpVersions=($(brew ls --versions | ggrep -E 'php(@.*)?\s' | ggrep -oP '(?<=\s)\d\.\d' | uniq | sort))

# create alias for every version of PHP installed with HomeBrew
for phpVersion in ${installedPhpVersions[*]}; do
    value="{"

    for otherPhpVersion in ${installedPhpVersions[*]}; do
        if [ "${otherPhpVersion}" = "${phpVersion}" ]; then
            continue
        fi

        # unlink other PHP version
        value="${value} brew unlink php@${otherPhpVersion};"
    done

    # link desired PHP version
    value="${value} brew link php@${phpVersion} --force --overwrite; brew services restart php@${phpVersion}; } &> /dev/null && php -v"

    alias "${phpVersion}"="${value}"
done

export PATH

# configure my multi-line prompt
PS1='\u\[\033[32m\] \W\[\033[33m\] $(git branch 2>/dev/null | grep '^*')\[\033[00m\] $ '
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
eval "$(starship init bash)"
starship_precmd_user_func="set-title"

source ~/.aliases

alias refresh="source .bashrc"

export PATH="/usr/local/bin/:$PATH"
export PATH="/usr/local/sbin:$PATH"
