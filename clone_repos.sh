#!/usr/bin/env bash

DIR=`dirname "$(readlink -f "$0")"`

cd $DIR

git clone git@bitbucket.org:i3logix/ballottrax_behat.git
git clone git@bitbucket.org:i3logix/ballottrax_build.git
git clone git@bitbucket.org:i3logix/ballottrax_db.git
git clone git@bitbucket.org:i3logix/ballottrax_files.git
git clone git@bitbucket.org:i3logix/ballottrax_scripts.git
git clone git@bitbucket.org:i3logix/ballottrax_web.git
git clone git@bitbucket.org:i3logix/slartybartfast.git

cd ballottrax_files
git lfs pull
