// This file can be used with the themer CLI, see https://github.com/mjswensen/themer

module.exports.colors = {
  "dark": {
    "shade0": "#5A5475",
    "shade7": "#FFFFFF",
    "accent0": "#FF857F",
    "accent1": "#F28144",
    "accent2": "#FFF352",
    "accent3": "#FFF352",
    "accent4": "#C2FFDF",
    "accent5": "#FFB8D1",
    "accent6": "#FFB8D1",
    "accent7": "#C2FFDF"
  },
  "light": {
    "shade0": "#FFFCFF",
    "shade1": "#E0DCE0",
    "shade2": "#C1BCC2",
    "shade3": "#A29DA3",
    "shade4": "#847E85",
    "shade5": "#656066",
    "shade6": "#474247",
    "shade7": "#282629",
    "accent0": "#F03E4D",
    "accent1": "#F37735",
    "accent2": "#EEBA21",
    "accent3": "#97BD2D",
    "accent4": "#1FC598",
    "accent5": "#53A6E1",
    "accent6": "#BF65F0",
    "accent7": "#EE4EB8"
  }
};

// Your theme's URL: https://themer.dev/?colors.dark.accent0=%23FF857F&colors.dark.accent1=%23F28144&colors.dark.accent2=%23FFF352&colors.dark.accent3=%23FFF352&colors.dark.accent4=%23C2FFDF&colors.dark.accent5=%23FFB8D1&colors.dark.accent6=%23FFB8D1&colors.dark.accent7=%23C2FFDF&colors.dark.shade0=%235A5475&colors.dark.shade1=%23474247&colors.dark.shade2=%23656066&colors.dark.shade3=%23E6C000&colors.dark.shade4=%23A29DA3&colors.dark.shade5=%23fff&colors.dark.shade6=%23fff&colors.dark.shade7=%23ffffff&colors.light.accent0=%23F03E4D&colors.light.accent1=%23F37735&colors.light.accent2=%23EEBA21&colors.light.accent3=%2397BD2D&colors.light.accent4=%231FC598&colors.light.accent5=%2353A6E1&colors.light.accent6=%23BF65F0&colors.light.accent7=%23EE4EB8&colors.light.shade0=%23FFFCFF&colors.light.shade1=%23E0DCE0&colors.light.shade2=%23C1BCC2&colors.light.shade3=%23A29DA3&colors.light.shade4=%23847E85&colors.light.shade5=%23656066&colors.light.shade6=%23474247&colors.light.shade7=%23282629&activeColorSet=dark&calculateIntermediaryShades.dark=true&calculateIntermediaryShades.light=false
