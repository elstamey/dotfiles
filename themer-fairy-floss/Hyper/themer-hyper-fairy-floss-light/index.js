
    module.exports.decorateConfig = config => {
      return Object.assign({}, config, {
        cursorColor: 'rgba(191, 101, 240, 0.5)',
        cursorAccentColor: '#fffcff',
        foregroundColor: '#474247',
        backgroundColor: '#fffcff',
        selectionColor: 'rgba(83, 166, 225, 0.09999999999999998)',
        borderColor: '#1fc598',
        colors: {
          black: '#fffcff',
          red: '#f03e4d',
          green: '#97bd2d',
          yellow: '#eeba21',
          blue: '#53a6e1',
          magenta: '#ee4eb8',
          cyan: '#1fc598',
          white: '#474247',
          lightBlack: '#e0dce0',
          lightRed: '#f37735',
          lightGreen: '#97bd2d',
          lightYellow: '#eeba21',
          lightBlue: '#53a6e1',
          lightMagenta: '#ee4eb8',
          lightCyan: '#1fc598',
          lightWhite: '#282629',
        },
      });
    };
  