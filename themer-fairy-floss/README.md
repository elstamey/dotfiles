# `themer`

Your theme's unique URL:

https://themer.dev/?colors.dark.accent0=%23FF857F&colors.dark.accent1=%23F28144&colors.dark.accent2=%23FFF352&colors.dark.accent3=%23FFF352&colors.dark.accent4=%23C2FFDF&colors.dark.accent5=%23FFB8D1&colors.dark.accent6=%23FFB8D1&colors.dark.accent7=%23C2FFDF&colors.dark.shade0=%235A5475&colors.dark.shade1=%23474247&colors.dark.shade2=%23656066&colors.dark.shade3=%23E6C000&colors.dark.shade4=%23A29DA3&colors.dark.shade5=%23fff&colors.dark.shade6=%23fff&colors.dark.shade7=%23ffffff&colors.light.accent0=%23F03E4D&colors.light.accent1=%23F37735&colors.light.accent2=%23EEBA21&colors.light.accent3=%2397BD2D&colors.light.accent4=%231FC598&colors.light.accent5=%2353A6E1&colors.light.accent6=%23BF65F0&colors.light.accent7=%23EE4EB8&colors.light.shade0=%23FFFCFF&colors.light.shade1=%23E0DCE0&colors.light.shade2=%23C1BCC2&colors.light.shade3=%23A29DA3&colors.light.shade4=%23847E85&colors.light.shade5=%23656066&colors.light.shade6=%23474247&colors.light.shade7=%23282629&activeColorSet=dark&calculateIntermediaryShades.dark=true&calculateIntermediaryShades.light=false

If you find `themer` useful, here are some ways to support the project:

* Star [`themer` on GitHub](https://github.com/mjswensen/themer)
* Follow [@themerdev](https://twitter.com/themerdev) on Twitter
* [Send a tip through the Brave Browser](https://brave.com/the537), either on [the repository page](https://github.com/mjswensen/themer) or [the Web UI](https://themer.dev)
* Pay what you want when downloading your theme from [themer.dev](https://themer.dev)

# Installation instructions

## Block Wave Wallpaper

Files generated:

* `Block Wave Wallpaper/themer-wallpaper-block-wave-dark-1471x903.svg`
* `Block Wave Wallpaper/themer-wallpaper-block-wave-light-1471x903.svg`

## Burst Wallpaper

Files generated:

* `Burst Wallpaper/themer-wallpaper-burst-dark-1471x903.svg`
* `Burst Wallpaper/themer-wallpaper-burst-light-1471x903.svg`

## Chrome

1. Launch Chrome and go to `chrome://extensions`.
2. Check the "Developer mode" checkbox at the top.
3. Click the "Load unpacked extension..." button and choose the desired theme directory (`Chrome/Themer Dark` or `Chrome/Themer Light`).

(To reset or remove the theme, visit `chrome://settings` and click "Reset to Default" in the "Appearance" section.)

## Diamonds Wallpaper

Files generated:

* `Diamonds Wallpaper/themer-wallpaper-diamonds-dark-1471x903.svg`
* `Diamonds Wallpaper/themer-wallpaper-diamonds-light-1471x903.svg`

## Firefox Add-on

To use the generated extension package, the code will need to be packaged up and signed by Mozilla.

To package the code in preparation for submission, the `web-ext` tool can be used:

    npx web-ext build --source-dir 'Firefox Add-on/Themer Dark' # or 'Firefox Add-on/Themer Light'

Then the package can be submitted to Mozilla for review in the [Add-on Developer Hub](https://addons.mozilla.org/en-US/developers/addon/submit/distribution).

Learn more about Firefox themes from [extensionworkshop.com](https://extensionworkshop.com/documentation/themes/)

To theme Firefox without the need to create a developer account and go through the extension review process, see themer's integration with [Firefox Color](https://color.firefox.com).

## Firefox Color

The Firefox Color add-on allows for simple theming without the need for a developer account or package review process by Mozilla.

1. Install the [Firefox Color add-on](https://addons.mozilla.org/en-US/firefox/addon/firefox-color/).
2. Open 'Firefox Color/themer-dark.url' or 'Firefox Color/themer-light.url' directly with Firefox.
3. Click "Yep" when prompted to apply the custom theme.

For a more fully featured Firefox theme, see themer's Firefox theme add-on generator.

## Hyper

First, copy (or symlink) the outputted package directories to the Hyper local plugins directory:

    cp -R 'Hyper/themer-hyper-dark' ~/.hyper_plugins/local/
    cp -R 'Hyper/themer-hyper-light' ~/.hyper_plugins/local/

Then edit `~/.hyper.js` and add the package to the `localPlugins` array:

    ...
    localPlugins: [
      'themer-hyper-dark' // or 'themer-hyper-light'
    ],
    ...

## iTerm

1. Launch iTerm
2. Press `command`-`I` to open the iTerm preferences
3. Choose Colors > Color Presets... > Import... and choose the generated theme file (`iTerm/themer-iterm-dark.itermcolors` or `iTerm/themer-iterm-light.itermcolors`)

## Octagon Wallpaper

Files generated:

* `Octagon Wallpaper/themer-wallpaper-octagon-dark-1471x903.svg`
* `Octagon Wallpaper/themer-wallpaper-octagon-light-1471x903.svg`

## Shirts Wallpaper

Files generated:

* `Shirts Wallpaper/themer-wallpaper-shirts-dark-1471-903.svg`
* `Shirts Wallpaper/themer-wallpaper-shirts-light-1471-903.svg`

## Slack sidebar

Copy the contents of `Slack sidebar/themer-slack-dark.txt` or `Slack sidebar/themer-slack-light.txt` and paste into the custom theme input in Slack's preferences.

## Triangles Wallpaper

Files generated:

* `Triangles Wallpaper/themer-wallpaper-triangles-dark-1471x903.svg`
* `Triangles Wallpaper/themer-wallpaper-triangles-light-1471x903.svg`

## Trianglify Wallpaper

Files generated:

* `Trianglify Wallpaper/themer-wallpaper-trianglify-dark-1471x903-0.75-1.svg`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-dark-1471x903-0.75-2.svg`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-light-1471x903-0.75-1.svg`
* `Trianglify Wallpaper/themer-wallpaper-trianglify-light-1471x903-0.75-2.svg`

## Windows Terminal

1. Open the Windows Terminal settings (`Ctrl`-`,`)
2. Add the contents of 'Windows Terminal/themer-dark.json' and 'Windows Terminal/themer-light.json' to the `schemes` array in `profile.json`
3. Set the `colorScheme` property to the desired scheme name ("Themer Dark" or "Themer Light") in the profiles section of `profile.json`, e.g.:

    "profiles": {
      "defaults": {
        "colorScheme": "Themer Dark"
      }
    }