#!/bin/bash
#
# Set Work Profile
# This sets workprofile for the current repo and all sub-repos
#
# Usage:
# $ ./set_work_profile.sh
#
# Output:
# repo_name [current_branch] current_email
#

    if [ "$(uname -s)" = 'Linux' ]; then
      DIR=`dirname "$(readlink -f "$0")"`
    else
      DIR=`dirname "$(readlink  "$0")"`
    fi

    RED='\033[0;31m'
    YELLOW='\033[0;33m'
    GREEN='\033[0;32m'
    NoColor='\033[0m'

    # Subshell so we don't end up in a different dir than where we started.
    (
	    (git workprofile)
        (cp ~/Code/dotfiles/prepare-commit-msg ./.git/hooks/prepare-commit-msg; chmod +x ./.git/hooks/prepare-commit-msg)
        cd "$DIR"
        for sub in *; do
            [[ -d "${sub}/.git" ]] || continue

            (cd "$sub"; git workprofile; )
            (cp ~/Code/dotfiles/prepare-commit-msg ./.git/hooks/prepare-commit-msg; chmod +x ./.git/hooks/prepare-commit-msg)
            PROFILE_EMAIL=$(cd "$sub"; git config user.email)

            if [ "$PROFILE_EMAIL" == 'estamey@i3logix.com' ]; then
                printf "${GREEN} ${PROFILE_EMAIL} ${NoColor} \n"
            else
                printf "${YELLOW} ${PROFILE_EMAIL}  ${NoColor}\n"
            fi
        done
    )
