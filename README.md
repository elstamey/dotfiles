profiles
========

install commandline tools

install homebrew

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"


install hyper terminal and starship prompt

    curl -sS https://starship.rs/install.sh | sh
    brew install starship


brew install

- fzf
- git-delta
- grep
- pcre2
- bash-completion
- zsh-completions
-   after installing zsh-completions, I updated the zshrc but needed these commands to handle an error involving insecure directories
-
      chmod go-w '/usr/local/share'
      chmod -R go-w '/usr/local/share/zsh'

my bash config files and git config

and phpstorm settings sync on and save .idea folder for each repo

curl:

- plug-vim

    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

- nvm

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

- php

  brew tap shivammathur/php
  brew install shivammathur/php/php@8.2
  brew link --overwrite --force php@8.2
  brew install shivammathur/php/php@8.1
  brew link --overwrite --force php@8.1
  brew install shivammathur/php/php@8.0
  brew link --overwrite --force php@8.0
  pecl install redis
  pecl install xdebug
  brew install php-redis


brew install:

- git-lfs
- php@8.0
- php@8.1
- php@8.2
-

composer:

[Source](https://getcomposer.org/download/)


vim config:

after copying the files
open vim and use :PlugInstall

vim config should get the gruvbox theme
gruvbox theme [link](https://github.com/morhetz/gruvbox)
