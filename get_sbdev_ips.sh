#!/bin/bash
#
# https://www.golinuxcloud.com/commands-check-if-connected-to-internet-shell/
#
#
#
# Get Sandbox IPs
# Get IP addresses from sandbox dev environment to update local /etc/hosts file
#
# Usage:`
# $ ./get_sbdev_ips.sh <sbdev_env_name> [<proj_name>]
#
# Output:
# ./get_sbdev_ips.sh emily-ballot ballot
#=======================================================================
#=    Checking emily-ballot sandbox environment
#=
#=    Replace the following in local /etc/hosts file
#=======================================================================
#
#172.21.78.27  web.ballottrax
#172.21.76.183  worker.ballottrax
#172.21.76.160  db.ballottrax
#172.21.75.38  xfer.ballottrax
#


    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[0;33m'
    NoColor='\033[0m'

    if [ "$1" = "" ]; then
      printf "${RED}1 argument required, $# provided${NoColor}\n\n"
      printf "Usage:\n"
      printf "# $ ./get_sbdev_ips.sh <sbdev_env_name> [<proj_name>]\n"
      exit
    fi

    if [ "$1" ]; then
      ENV_NAME="$1"
    else
      ENV_NAME="test-ballot"
    fi

    if [ "$2" ]; then
      PROJ_NAME="$2"
    else
      PROJ_NAME="ballot"
    fi

    if [ "$PROJ_NAME" = "ballot" ]; then
      HOST_NAME="ballottrax"
    else
      HOST_NAME="$PROJ_NAME"
    fi

    # Subshell so we don't end up in a different dir than where we started.
    (


            printf "=======================================================================\n"
            printf "=    Checking ${ENV_NAME} sandbox environment ${NoColor}\n"
            printf "=    Checking ${PROJ_NAME} project ${NoColor}\n"
            printf "=\n"
            printf "=    Replace the following in local /etc/hosts file\n"
            printf "=======================================================================\n\n"

            IP_ADDR=$(dig +short "https://${ENV_NAME}-sbdev.i3${PROJ_NAME}.net")
            printf "${IP_ADDR}  dev.${HOST_NAME} #load balanced server\n"

            IP_ADDR=$(dig +short "${ENV_NAME}.web.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
            printf "${IP_ADDR}  web.${HOST_NAME}\n"

            IP_ADDR=$(dig +short "${ENV_NAME}.worker.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
            printf "${IP_ADDR}  worker.${HOST_NAME}\n"

            IP_ADDR=$(dig +short "${ENV_NAME}.db.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
            printf "${IP_ADDR}  db.${HOST_NAME}\n"

            if [ "$PROJ_NAME" = "ballot" ]; then
              IP_ADDR=$(dig +short "${ENV_NAME}.xfer.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
              printf "${IP_ADDR}  xfer.${HOST_NAME}\n"
            else
              IP_ADDR=$(dig +short "${ENV_NAME}.dms.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
              printf "${IP_ADDR}  dms.${HOST_NAME}\n"

              IP_ADDR=$(dig +short "${ENV_NAME}.reports.sbdev.${PROJ_NAME}.oregonaws.i3logix.com")
              printf "${IP_ADDR}  reports.${HOST_NAME}\n"
            fi
    )
