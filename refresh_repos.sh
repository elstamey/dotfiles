#!/bin/bash
#
# https://www.golinuxcloud.com/commands-check-if-connected-to-internet-shell/
#
#
#
# Refresh Repos
# Refreshes the project repos based on either the optional branch name
# provided or the develop by default
#
# Usage:
# $ ./refresh_repos.sh [<branch_name>]
#
# Output:
#  =======================================================================
#  =    On ballottrax_web[master]
#  =======================================================================
#  This repo doesn't have a develop branch and should default to master.
#  Already up to date.
#  ballottrax_web [master]
#
# Note: If there are conflicts, no changes will be made in the repo
#
    if [ "$(uname -s)" = 'Linux' ]; then
      DIR=`dirname "$(readlink -f "$0")"`
    else
      DIR=`dirname "$(readlink  "$0")"`
    fi

    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[0;33m'
    NoColor='\033[0m'

    if [ -z "$1" ] ; then
        printf "No arguments provided.  Defaulting to ${GREEN}master${NoColor}\n"
        DESIRED_BRANCH="master"
    else
        DESIRED_BRANCH=$1
    fi

    if [ -z "$2" ] ; then
        NEW_PROFILE=false
    else
        NEW_PROFILE=true
    fi

    # Subshell so we don't end up in a different dir than where we started.
    (

        for subrepo in *; do
            [[ -d "${subrepo}/.git" ]] || continue

            if [ "$NEW_PROFILE" == true ] ; then
                (cd "$subrepo"; git workprofile)
            fi

            CURRENT_BRANCH_NAME=`cd "$subrepo"; git  branch | grep '^\*' | cut -d' ' -f2`

            if [ "$DESIRED_BRANCH" == "master" ] && [ "$subrepo" == "ballottraxer" ] ;  then
                DESIRED_BRANCH="main"
            fi

            printf "=======================================================================\n"

            if [ "$CURRENT_BRANCH_NAME" != "$DESIRED_BRANCH" ]; then
              printf "=    ${YELLOW}On $subrepo [${CURRENT_BRANCH_NAME}] ${NoColor}\n"
              printf "=======================================================================\n"
              printf "${RED}Switching to [${DESIRED_BRANCH}] from [${CURRENT_BRANCH_NAME}] ${NoColor}\n"
            else
              printf "=    On $subrepo [${CURRENT_BRANCH_NAME}] \n"
              printf "=======================================================================\n"
              printf "Checking [origin/${DESIRED_BRANCH}] from [${CURRENT_BRANCH_NAME}] ${NoColor}\n"
            fi

            (cd "$subrepo"; git fetch --all --quiet; git checkout --quiet "$DESIRED_BRANCH"; git pull --ff-only; git checkout --quiet "$CURRENT_BRANCH_NAME")

            if [ "$DESIRED_BRANCH" == "main" ] && [ "$subrepo" == "ballottraxer" ] ;  then
                DESIRED_BRANCH="master"
            fi


        done
    )
