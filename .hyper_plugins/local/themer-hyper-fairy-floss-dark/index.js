
    module.exports.decorateConfig = config => {
      return Object.assign({}, config, {
        cursorColor: 'rgba(255, 184, 209, 0.5)',
        cursorAccentColor: '#5a5475',
        foregroundColor: '#e7e7eb',
        backgroundColor: '#5a5475',
        selectionColor: 'rgba(255, 184, 209, 0.09999999999999998)',
        borderColor: '#c2ffdf',
        colors: {
          black: '#5a5475',
          red: '#ff857f',
          green: '#fff352',
          yellow: '#fff352',
          blue: '#a5aaf2',
          magenta: '#f56dfc',
          cyan: '#c2ffdf',
          white: '#e7e7eb',
          lightBlack: '#726c89',
          lightRed: '#f28144',
          lightGreen: '#fff352',
          lightYellow: '#fff352',
          lightBlue: '#ffb8d1',
          lightMagenta: '#c2ffdf',
          lightCyan: '#c2ffdf',
          lightWhite: '#ffffff',
        },
    });
    };
  