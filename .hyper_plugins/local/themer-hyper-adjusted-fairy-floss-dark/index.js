
    module.exports.decorateConfig = config => {
      return Object.assign({}, config, {
        cursorColor: 'rgba(255, 184, 209, 0.5)',
        cursorAccentColor: '#5a5475',
        foregroundColor: '#e7e7eb',
        backgroundColor: '#8d83bc',
        selectionColor: 'rgba(255, 184, 209, 0.09999999999999998)',
        borderColor: 'rgba(194,255,223,0.84)',
        colors: {
          black: '#5a5475',
          red: '#ff857f',
          green: '#58cb56',
          yellow: '#fff352',
          blue: '#6aaac2',
          magenta: '#8f41ad',
          cyan: '#c2ffdf',
          white: '#e7e7eb',
          lightBlack: '#726c89',
          lightRed: '#d98b87',
          lightGreen: '#53a051',
          lightYellow: '#b5ae4e',
          lightBlue: '#628693',
          lightMagenta: '#6c3f7d',
          lightCyan: '#b1d6c2',
          lightWhite: '#ffffff',
        },
      });
    };
  