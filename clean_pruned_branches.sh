#!/bin/bash
#
#
#
#
# Clean Local Pruned Branches
#
# Usage:
# $ ./clean_pruned_branches.sh
#
# Output:
#  =======================================================================
#  =    On ballottrax_web[master]
#  =======================================================================
#  Already up to date.
#  ballottrax_web [master]
#
#    if [ "$(uname -s)" = 'Linux' ]; then
#      DIR=`dirname "$(readlink -f "$0")"`
#    else
#      DIR=`dirname "$(readlink  "$0")"`
#    fi

    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[0;33m'
    NoColor='\033[0m'

#    if [ -z "$1" ] ; then
#        printf "No arguments provided.  Defaulting to ${GREEN}master${NoColor}\n"
#        DESIRED_BRANCH="master"
#    else
#        DESIRED_BRANCH=$1
#    fi
#
#    if [ -z "$2" ] ; then
#        NEW_PROFILE=false
#    else
#        NEW_PROFILE=true
#    fi

    # Subshell so we don't end up in a different dir than where we started.
    (

        PRUNED_BRANCH_NAMES=$(git localmerged | cut -d' ' -f2)

        for prunedBranch in $PRUNED_BRANCH_NAMES; do

          printf "=======================================================================\n"
          printf "=    ${NoColor} $prunedBranch ${NoColor}\n"
          printf "=======================================================================${NoColor}\n"

          SOFT_DELETE_RESULT=$(git branch -d ${prunedBranch} 2>&1)
          if [[ $SOFT_DELETE_RESULT =~ ^"error".* ]]
          then
#            if [ -z "$1" ] ; then
#              printf "No arguments provided.\n"
#
#            else
#              printf "arg given ${1}"
#            fi
            printf "${NoColor}  ${SOFT_DELETE_RESULT} ${NoColor}\n\n"
            printf "${YELLOW}  Do you want to delete the branch?  [y/n] ${NoColor}\n"

            while true; do
              read -p ' > [y/n]: ' yn
              case $yn in
                [Yy]* ) printf "${GREEN}"; git branch -D ${prunedBranch}; printf "${NoColor}\n\n"; break;;
                [Nn]* ) printf "${YELLOW} not deleting ${NoColor}\n\n "; break;;
                [Qq]* ) exit;;
                * ) echo "${RED} Please answer yes, no, or quit"
              esac
            done
          else
            printf "${RED}  not an error ${NoColor}\n\n"
            printf "${RED}  ${SOFT_DELETE_RESULT} ${NoColor}\n\n"
          fi

        done
    )
