(
  cp ~/.aliases .
  cp ~/.bashrc .
  cp ~/.bash_profile .
  cp -a ~/.config/* ./.config/
  cp ~/.dircolors .
  cp ~/.git-completion.bash .
  cp ~/.gitconfig .
  cp ~/.gitignore_global .
  cp ~/.hyper.js .
  cp -a ~/.hyper_plugins/* ./.hyper_plugins/
  cp ~/.ssh/config ./.ssh/
  cp ~/starship.toml .
  cp ~/.vim/plugins.vim ./.vim/
  cp ~/.vim/plugin-config.vim ./.vim/
  cp -a ~/.vim/colors/* ./.vim/colors/
  cp ~/.vimrc .
  cp -a ~/.zsh/*.zsh ./.zsh/
  cp ~/.zshrc .
  cp ~/dircolors-solarized.ansi-dark .
  cp ~/dircolors.gruvbox.dircolors .
#  cp ~/Code/sandbox_ballot/refresh_repos.sh .
#  cp ~/Code/sandbox_ballot/clone_repos.sh .
#  cp ~/Code/sandbox_ballot/get_sbdev_ips.sh .
#  cp ~/Code/sandbox_ballot/clean_pruned_branches.sh .
)
